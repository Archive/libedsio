/* -*-Mode: C;-*-
 * $Id: digest.c 1.5 Mon, 03 May 1999 16:13:31 -0700 jmacd $
 *
 * Copyright (C) 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include "edsio.h"
#include "edsiopriv.h"
#include "edsiostdio.h"

gint
edsio_md5_equal (gconstpointer   v,
		 gconstpointer   v2)
{
  return memcmp (v, v2, 16) == 0;
}

guint
edsio_md5_hash  (gconstpointer   v)
{
  guint8* md5 = (guint8*) v;
  guint x = 0;
  gint i, j;

  for (i = 0, j = 0; i < 16; i += 1, j += 1, j %= sizeof (guint))
    x ^= md5[i] << (8*j);

  return x;
}

void
edsio_digest_to_string (const MessageDigest* md, const guint8* digest_in, char *buf_out)
{
  gint i;

  for (i = 0; i < md->cksum_len; i += 1)
    sprintf (buf_out + 2*i, "%02x", digest_in[i]);
}

void
edsio_md5_to_string (const guint8* md5, char buf[33])
{
  return edsio_digest_to_string (edsio_message_digest_md5 (), md5, buf);
}

static gboolean
from_hex (char c, int* x, const char* ctx)
{
  char buf[2];

  if (c >= '0' && c <= '9')
    {
      (*x) = c - '0';
      return TRUE;
    }
  else if (c >= 'A' && c <= 'F')
    {
      (*x) = c - 'A' + 10;
      return TRUE;
    }
  else if (c >= 'a' && c <= 'f')
    {
      (*x) = c - 'a' + 10;
      return TRUE;
    }

  buf[0] = c;
  buf[1] = 0;

  edsio_generate_stringstring_event (EC_EdsioInvalidHexDigit, buf, ctx);
  return FALSE;
}

gboolean
edsio_md5_from_string (guint8* md5, const char buf[33])
{
  return edsio_digest_from_string (edsio_message_digest_md5 (), md5, buf);
}

gboolean
edsio_digest_from_string (const MessageDigest* md, guint8* digest_out, const char *str_in)
{
  gint i;
  gint l = strlen (str_in);

  if (l < 2*md->cksum_len)
    {
      edsio_generate_stringstring_event (EC_EdsioDigestStringShort, md->name, str_in);
      return FALSE;
    }
  else if (l > 2*md->cksum_len)
    {
      edsio_generate_stringstring_event (EC_EdsioDigestStringLong, md->name, str_in);
      return FALSE;
    }

  for (i = 0; i < md->cksum_len; i += 1)
    {
      char c1 = str_in[(2*i)];
      char c2 = str_in[(2*i)+1];
      int x1, x2;

      if (! from_hex (c1, &x1, str_in))
	return FALSE;

      if (! from_hex (c2, &x2, str_in))
	return FALSE;

      digest_out[i] = (x1 << 4) | x2;
    }

  return TRUE;
}

#define NMDS 2

static const MessageDigest _mds[2] = {
  {
    sizeof (EdsioMD5Ctx),
    16,
    0,
    "MD5",
    edsio_md5_init,
    edsio_md5_update,
    edsio_md5_final
  },
  {
    sizeof (EdsioSHACtx),
    20,
    1,
    "SHA",
    edsio_sha_init,
    edsio_sha_update,
    edsio_sha_final
  }
};

const MessageDigest*
edsio_message_digest_md5 (void)
{
  return _mds + 0;
}

const MessageDigest*
edsio_message_digest_sha (void)
{
  return _mds + 1;
}

const MessageDigest*
edsio_message_digest_by_name (const char* name)
{
  gint i;

  for (i = 0; i < NMDS; i += 1)
    {
      if (strcmp (name, _mds[i].name) == 0)
	return _mds + i;
    }

  edsio_generate_string_event (EC_EdsioMissingMessageDigest, name);
  return NULL;
}

const MessageDigest*
edsio_message_digest_by_id (guint id)
{
  g_assert (id < NMDS);

  return _mds + id;
}

static const guint8*
handle_digest_fault (FileHandle* fh, MessageDigestCtx *mdctx)
{
  guint8 buf[8192];
  guint file_len = file_position_to_abs (& fh->fh_file_len);
  guint file_pos = file_position_to_abs (& fh->fh_cur_pos);

  if (mdctx->update_pos < file_len)
    {
      if (mdctx->update_pos < file_pos)
	edsio_generate_handle_event (EC_EdsioDigestFault, fh);

      if (handle_seek (fh, mdctx->update_pos, HANDLE_SEEK_SET) != mdctx->update_pos)
	return NULL;

      for (;;)
	{
	  gssize res = handle_read (fh, buf, 8192);

	  if (res < 0)
	    return NULL;

	  if (res < 8192)
	    break;
	}
    }

  g_assert (! mdctx->finalized);

  g_assert (mdctx->update_pos == file_len);

  mdctx->finalized = TRUE;
  mdctx->md->final (mdctx->md_val, mdctx->md_ctx);

  return mdctx->md_val;
}

static const guint8*
handle_digest_finish (FileHandle* fh, MessageDigestCtx* mdctx)
{
  if (mdctx->finalized)
    return mdctx->md_val;

  if (fh->fh_open_mode == HV_Replace)
    {
      edsio_generate_handle_event (EC_EdsioDigestNotFinished, fh);
      return NULL;
    }
  else
    {
      return handle_digest_fault (fh, mdctx);
    }
}

const guint8*
handle_digest (FileHandle *fh, const MessageDigest *md)
{
  MessageDigestCtx* mdctx;

  for (mdctx = fh->fh_digests; mdctx; mdctx = mdctx->next)
    {
      if (mdctx->md == md)
	return handle_digest_finish (fh, mdctx);
    }

  if (fh->fh_open_mode == HV_Read)
    {
      if (! handle_digest_compute (fh, md))
	return NULL;

      return handle_digest_finish (fh, fh->fh_digests);
    }

  edsio_generate_handle_event (EC_EdsioDigestNotComputed, fh);
  return NULL;
}

gboolean
handle_digest_compute (FileHandle *fh, const MessageDigest *md)
{
  MessageDigestCtx* mdctx;

  if (file_position_to_abs (& fh->fh_cur_pos) != 0)
    {
      edsio_generate_handle_event (EC_EdsioDigestComputationLate, fh);
      return FALSE;
    }

  for (mdctx = fh->fh_digests; mdctx; mdctx = mdctx->next)
    {
      if (mdctx->md == md)
	return TRUE;
    }

  mdctx = g_new0 (MessageDigestCtx, 1);

  mdctx->next = fh->fh_digests;
  fh->fh_digests = mdctx;

  mdctx->md = md;
  mdctx->md_ctx = g_malloc (md->ctx_size);
  mdctx->md_val = g_malloc (md->cksum_len);

  md->init (mdctx->md_ctx);

  return TRUE;
}

gboolean
handle_digest_close (FileHandle* fh)
{
  MessageDigestCtx* mdctx;

  for (mdctx = fh->fh_digests; mdctx; mdctx = mdctx->next)
    {
      if (mdctx->finalized)
	continue;

      if (fh->fh_open_mode == HV_Replace)
	{
	  mdctx->finalized = TRUE;
	  mdctx->md->final (mdctx->md_val, mdctx->md_ctx);
	}
      else
	{
	  if (! handle_digest_finish (fh, mdctx))
	    return FALSE;
	}
    }

  return TRUE;
}
