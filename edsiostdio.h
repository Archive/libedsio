/* -*-Mode: C;-*-
 * $Id: edsiostdio.h 1.2 Wed, 27 Oct 1999 00:30:30 -0700 jmacd $
 *
 * Copyright (C) 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _EDSIOSTDIO_H_
#define _EDSIOSTDIO_H_

#include <stdio.h>

extern FileHandle* _stdout_handle;
extern FileHandle* _stdin_handle;
extern FileHandle* _stderr_handle;

/* It assumes these are not seekable */
FileHandle* handle_read_file   (const char* name);
FileHandle* handle_write_file  (const char* name);

FileHandle* handle_read_stdio  (FILE* f, const char* name);
FileHandle* handle_write_stdio (FILE* f, const char* name);

FileHandle* handle_read_fd     (int fd, const char* name);
FileHandle* handle_write_fd    (int fd, const char* name);

#endif
