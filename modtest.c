/* -*- Mode: C;-*-
 *
 * This file is part of XDelta - A binary delta generator.
 *
 * Copyright (C) 1997, 1998  Josh MacDonald
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 *
 * $Id: modtest.c 1.1 Tue, 06 Apr 1999 23:40:43 -0700 jmacd $
 */

#include <glib.h>
#include <gmodule.h>
#include "modtest_edsio.h"

G_MODULE_EXPORT const gchar*
g_module_check_init (GModule *module)
{
  g_print ("modtest: check-init\n");

  return NULL;
}

G_MODULE_EXPORT void
g_module_unload (GModule *module)
{
  g_print ("modtest: unloaded\n");
}
