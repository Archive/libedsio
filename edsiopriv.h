/* -*-Mode: C;-*-
 * $Id: edsiopriv.h 1.2 Mon, 12 Apr 1999 00:54:19 -0700 jmacd $
 *
 * Copyright (C) 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#ifndef _EDSIOPRIV_H_
#define _EDSIOPRIV_H_

gboolean edsio_stdio_init (void);

gboolean handle_digest_close (FileHandle* fh);

struct _MessageDigestCtx {

  MessageDigestCtx    *next;
  const MessageDigest *md;
  void                *md_ctx;
  void                *md_val;
  guint                update_pos;
  guint                finalized : 1;
};

#endif
