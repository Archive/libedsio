/* -*-Mode: C;-*-
 * $Id: fh.c 1.14 Tue, 04 Jan 2000 17:19:17 -0800 jmacd $
 *
 * Copyright (C) 1998, 1999, Josh MacDonald.
 * All Rights Reserved.
 *
 * Author: Josh MacDonald <jmacd@CS.Berkeley.EDU>
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "edsio.h"
#include "edsiopriv.h"

static gboolean handle_unmap_current_page (FileHandle* fh);
static gssize   handle_map_current_page   (FileHandle* fh);


/* Handle source/sink impls
 */
typedef struct _HandleSerialSource HandleSerialSource;
typedef struct _HandleSerialSink HandleSerialSink;

struct _HandleSerialSource {
  SerialSource source;

  FileHandle* fh;
};

struct _HandleSerialSink {
  SerialSink sink;

  FileHandle* fh;
  gpointer data1;
  gpointer data2;
  gpointer data3;
  gboolean (* cont_onclose) (gpointer data1, gpointer data2, gpointer data3);
};

static SerialType handle_source_type           (SerialSource* source, gboolean set_allocation);
static gboolean   handle_source_close          (SerialSource* source);
static gboolean   handle_source_read           (SerialSource* source, guint8 *ptr, guint32 len);
static void       handle_source_free           (SerialSource* source);

static gboolean     handle_sink_type             (SerialSink* sink, SerialType type, guint len, gboolean set_allocation);
static gboolean     handle_sink_close            (SerialSink* sink);
static gboolean     handle_sink_write            (SerialSink* sink, const guint8 *ptr, guint32 len);
static void         handle_sink_free             (SerialSink* sink);

SerialSource*
handle_source (FileHandle* fh)
{
  HandleSerialSource* it = g_new0 (HandleSerialSource, 1);

  serializeio_source_init (&it->source,
			   handle_source_type,
			   handle_source_close,
			   handle_source_read,
			   handle_source_free,
			   NULL,
			   NULL);

  it->fh = fh;

  return &it->source;
}

SerialSink*
handle_sink (FileHandle* fh, gpointer data1, gpointer data2, gpointer data3, gboolean (* cont_onclose) (gpointer data1, gpointer data2, gpointer data3))
{
  HandleSerialSink* it = g_new0 (HandleSerialSink, 1);

  serializeio_sink_init (&it->sink,
			 handle_sink_type,
			 handle_sink_close,
			 handle_sink_write,
			 handle_sink_free,
			 NULL);

  it->fh = fh;
  it->data1 = data1;
  it->data2 = data2;
  it->data3 = data3;
  it->cont_onclose = cont_onclose;

  return &it->sink;
}

SerialType
handle_source_type (SerialSource* source, gboolean set_allocation)
{
  HandleSerialSource* ssource = (HandleSerialSource*) source;
  guint32 x;

  if (! handle_getui (ssource->fh, &x))
    return ST_Error;

  if (set_allocation)
    {
      if (! handle_getui (ssource->fh, &source->alloc_total))
	return ST_Error;
    }

  return x;
}

gboolean
handle_source_close (SerialSource* source)
{
  HandleSerialSource* ssource = (HandleSerialSource*) source;

  return handle_close (ssource->fh);
}

gboolean
handle_source_read (SerialSource* source, guint8 *ptr, guint32 len)
{
  HandleSerialSource* ssource = (HandleSerialSource*) source;

  return handle_read (ssource->fh, ptr, len) == len;
}

void
handle_source_free (SerialSource* source)
{
  g_free (source);
}

gboolean
handle_sink_type (SerialSink* sink, SerialType type, guint len, gboolean set_allocation)
{
  HandleSerialSink* ssink = (HandleSerialSink*) sink;

  if (! handle_putui (ssink->fh, type))
    return FALSE;

  if (set_allocation && ! handle_putui (ssink->fh, len))
    return FALSE;

  return TRUE;
}

gboolean
handle_sink_close (SerialSink* sink)
{
  HandleSerialSink* ssink = (HandleSerialSink*) sink;

  if (handle_close (ssink->fh))
    {
      if (ssink->cont_onclose)
	return ssink->cont_onclose (ssink->data1, ssink->data2, ssink->data3);

      return TRUE;
    }

  return FALSE;
}

gboolean
handle_sink_write (SerialSink* sink, const guint8 *ptr, guint32 len)
{
  HandleSerialSink* ssink = (HandleSerialSink*) sink;

  return handle_write (ssink->fh, ptr, len);
}

void
handle_sink_free (SerialSink* sink)
{
  g_free (sink);
}

/* Table-dependent handle functions
 */

void
handle_free (FileHandle *fh)
{
  MessageDigestCtx* mdctx, *tmp;
  HookList *hl, *tmp2;

  g_assert (! fh->fh_cur_page);

  if (fh->fh_read_page)
    g_free (fh->fh_read_page);

  for (mdctx = fh->fh_digests; mdctx; )
    {
      tmp = mdctx;
      mdctx = mdctx->next;
      g_free (tmp->md_ctx);
      g_free (tmp->md_val);
      g_free (tmp);
    }

  for (hl = fh->fh_close_hooks; hl; )
    {
      tmp2 = hl;
      hl = hl->next;
      g_free (tmp2);
    }

  return (* fh->table->fh_free) (fh);
}

void
handle_close_hook (FileHandle *fh, void* data, gboolean (* func) (FileHandle* fh, void* data))
{
  HookList *hl = g_new (HookList, 1);

  hl->data = data;
  hl->func = func;
  hl->next = fh->fh_close_hooks;
  fh->fh_close_hooks = hl;
}

gboolean
handle_close (FileHandle *fh)
{
  HookList *hl;

  g_return_val_if_fail (fh, FALSE);

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return FALSE;
    }

  if (! handle_unmap_current_page (fh))
    return FALSE;

  if (! handle_digest_close (fh))
    return FALSE;

  fh->fh_closed = TRUE;

  if (! (* fh->table->fh_close) (fh))
    return FALSE;

  for (hl = fh->fh_close_hooks; hl; hl = hl->next)
    {
      if (! hl->func (fh, hl->data))
	return FALSE;
    }

  return TRUE;
}

void
digest (FileHandle* fh, const guint8* buf, guint offset, guint len)
{
  MessageDigestCtx* mdctx;

  if (len == 0)
    return;

  for (mdctx = fh->fh_digests; mdctx; mdctx = mdctx->next)
    {
      if (offset == mdctx->update_pos)
	{
	  mdctx->update_pos += len;
	  mdctx->md->update (mdctx->md_ctx, buf, len);
	}
    }
}

void
handle_abort (FileHandle *fh)
{
  g_return_if_fail (fh);

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return;
    }

  fh->fh_closed = TRUE;

  handle_unmap_current_page (fh);

  (* fh->table->fh_abort) (fh);
}

gboolean
handle_unmap_current_page (FileHandle* fh)
{
  gssize rem;

  if (! fh->fh_cur_page)
    return TRUE;

  g_assert (! fh->table->fh_read);

  rem = file_position_rem_on_page_no (&fh->fh_file_len, fh->fh_cur_pos.page);

  g_assert (rem >= 0);

  if (fh->fh_open_mode == HV_Replace)
    digest (fh, fh->fh_cur_page, fh->fh_cur_pos.page * fh->fh_cur_pos.page_size, rem);

  return (* fh->table->fh_unmap) (fh, fh->fh_cur_pos.page, (const guint8**) & fh->fh_cur_page);
}

gssize
handle_map_current_page (FileHandle* fh)
{
  gssize rem, rem_on_page;

  g_assert (! fh->table->fh_read);

  rem_on_page = file_position_rem_on_page_no (&fh->fh_file_len, fh->fh_cur_pos.page);
  rem = rem_on_page - fh->fh_cur_pos.offset;

  g_assert (rem >= 0);

  if (rem > 0 && fh->fh_cur_page != NULL)
    return rem;

  if (rem == 0 && fh->fh_cur_pos.page == fh->fh_file_len.page)
    return 0;

  if (! handle_unmap_current_page (fh))
    return -1;

  g_assert (! fh->fh_cur_page);

  if (rem == 0)
    {
      fh->fh_cur_pos.offset = 0;
      fh->fh_cur_pos.page += 1;

      rem_on_page = file_position_rem_on_page_no (&fh->fh_file_len, fh->fh_cur_pos.page);
      rem = rem_on_page - fh->fh_cur_pos.offset;
    }

  if ((* fh->table->fh_map) (fh, fh->fh_cur_pos.page, (const guint8**) & fh->fh_cur_page) != rem_on_page)
    return -1;

  if (fh->fh_open_mode == HV_Read)
    digest (fh, fh->fh_cur_page, fh->fh_cur_pos.page * fh->fh_cur_pos.page_size, rem_on_page);

  return rem;
}

gssize
handle_read (FileHandle *fh,
	     guint8     *buf,
	     gsize       nbyte0)
{
  gsize nbyte = nbyte0;
  gssize nread;

  g_return_val_if_fail (fh && buf, -1);

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return -1;
    }

  if (fh->fh_open_mode != HV_Read)
    {
      edsio_generate_handlestring_event (EC_EdsioInvalidHandleMode, fh, "read");
      return -1;
    }

  if (nbyte == 0)
    return 0;

  if (fh->table->fh_read)
    {
      nread = (* fh->table->fh_read) (fh, buf, nbyte);

      if (nread > 0)
	{
	  digest (fh, buf, file_position_to_abs (& fh->fh_cur_pos), nread);

	  fh->fh_cur_pos.offset += nread;

	  file_position_from_abs (fh->fh_cur_pos.page_size,
				  file_position_to_abs (& fh->fh_cur_pos),
				  &fh->fh_cur_pos);
	}

      return nread;
    }

  while (nbyte > 0)
    {
      gssize rem = handle_map_current_page (fh);
      gsize cp;

      if (rem < 0)
	return -1;

      if (rem > 0)
	{
	  cp = MIN (rem, nbyte);

	  memcpy (buf, fh->fh_cur_page + fh->fh_cur_pos.offset, cp);

	  fh->fh_cur_pos.offset += cp;

	  nbyte -= cp;
	  buf += cp;
	}

      if (rem == 0)
	break;
    }

  return nbyte0 - nbyte;
}

gboolean
handle_write (FileHandle *fh,
	      const guint8 *buf,
	      gsize nbyte)
{
  gssize len;
  gssize pos;

  g_return_val_if_fail (fh, FALSE);

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return FALSE;
    }

  if (fh->fh_open_mode != HV_Replace)
    {
      edsio_generate_handlestring_event (EC_EdsioInvalidHandleMode, fh, "replace");
      return FALSE;
    }

  if (nbyte == 0)
    return TRUE;
  else
    g_assert (buf);

  g_assert (fh->fh_has_len);

  len = file_position_to_abs (&fh->fh_file_len);
  pos = file_position_to_abs (&fh->fh_cur_pos);

  len = MAX (len, pos + nbyte);

  file_position_from_abs (fh->fh_file_len.page_size, len, &fh->fh_file_len);

  if (fh->table->fh_write)
    {
      gboolean res = (* fh->table->fh_write) (fh, buf, nbyte);

      if (res)
	{
	  digest (fh, buf, file_position_to_abs (& fh->fh_cur_pos), nbyte);

	  fh->fh_cur_pos.offset += nbyte;

	  file_position_from_abs (fh->fh_cur_pos.page_size,
				  file_position_to_abs (& fh->fh_cur_pos),
				  &fh->fh_cur_pos);
	}

      return res;
    }

  while (nbyte > 0)
    {
      gssize rem = handle_map_current_page (fh);
      gsize cp;

      if (rem < 0)
	return FALSE;

      g_assert (rem > 0);

      cp = MIN (rem, nbyte);

      /* same as above except switch buffers. */
      g_assert (fh->fh_cur_page);
      memcpy (fh->fh_cur_page + fh->fh_cur_pos.offset, buf, cp);

      fh->fh_cur_pos.offset += cp;

      nbyte -= cp;
      buf += cp;
    }

  return TRUE;
}

gssize
handle_map_page (FileHandle *fh, guint pgno, const guint8** mem)
{
  gssize res;

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return -1;
    }

  if (fh->fh_open_mode != HV_Read)
    {
      edsio_generate_handlestring_event (EC_EdsioInvalidHandleMode, fh, "map");
      return -1;
    }

  if (fh->table->fh_read)
    {
      if (fh->fh_cur_pos.page != pgno || fh->fh_cur_pos.offset != 0)
	{
	  edsio_generate_handle_event (EC_EdsioIllegalMapSeek, fh);
	  return -1;
	}

      if (fh->fh_read_mapped)
	{
	  edsio_generate_handle_event (EC_EdsioIllegalMapUnmapFirst, fh);
	  return -1;
	}

      if (! fh->fh_read_page)
	fh->fh_read_page = g_malloc (fh->fh_cur_pos.page_size);

      if ((res = (* fh->table->fh_read) (fh, fh->fh_read_page, fh->fh_cur_pos.page_size)) < 0)
	return -1;

      fh->fh_read_mapped = TRUE;

      digest (fh, fh->fh_read_page, file_position_to_abs (& fh->fh_cur_pos), res);

      fh->fh_cur_pos.offset += res;

      file_position_from_abs (fh->fh_cur_pos.page_size,
			      file_position_to_abs (& fh->fh_cur_pos),
			      &fh->fh_cur_pos);

      (* mem) = fh->fh_read_page;

      return res;
    }

  if (pgno > fh->fh_file_len.page)
    {
      edsio_generate_handleint_event (EC_EdsioInvalidPage, fh, pgno);
      return -1;
    }

  res = (* fh->table->fh_map) (fh, pgno, mem);

  if (res > 0)
    digest (fh, *mem, pgno * fh->fh_cur_pos.page_size, res);

  return res;
}

gboolean
handle_unmap_page (FileHandle *fh, guint pgno, const guint8** mem0_ptr)
{
  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return FALSE;
    }

  if (fh->fh_open_mode != HV_Read)
    {
      edsio_generate_handlestring_event (EC_EdsioInvalidHandleMode, fh, "unmap");
      return FALSE;
    }

  if (fh->table->fh_read)
    {
      if (! fh->fh_read_mapped)
	{
	  edsio_generate_handle_event (EC_EdsioIllegalUnmap, fh);
	  return -1;
	}

      fh->fh_read_mapped = FALSE;

      if (mem0_ptr)
	(* mem0_ptr) = NULL;

      return TRUE;
    }

  if (pgno > fh->fh_file_len.page)
    {
      edsio_generate_handleint_event (EC_EdsioInvalidPage, fh, pgno);
      return FALSE;
    }

  return (* fh->table->fh_unmap) (fh, pgno, mem0_ptr);
}

const char*
eventdelivery_handle_to_string (FileHandle* fh)
{
  g_return_val_if_fail (fh, g_strdup ("*error*"));

  return fh->table->fh_name (fh);
}

gssize
handle_seek (FileHandle *fh,
	     gssize offset,
	     gint whence)
{
  gssize cur;

  g_return_val_if_fail (fh, FALSE);
  g_return_val_if_fail (whence == HANDLE_SEEK_SET ||
			whence == HANDLE_SEEK_CUR ||
			whence == HANDLE_SEEK_END, FALSE);

  if (fh->fh_closed)
    {
      edsio_generate_handle_event (EC_EdsioHandleClosed, fh);
      return -1;
    }

  switch (whence)
    {
    case HANDLE_SEEK_SET:
      {
	cur = offset;
      }
      break;
    case HANDLE_SEEK_CUR:
      {
	cur = file_position_to_abs (&fh->fh_cur_pos);
	cur += offset;
      }
      break;
    case HANDLE_SEEK_END:
      {
	if (! fh->fh_has_len)
	  {
	    edsio_generate_handle_event (EC_EdsioHandleHasNoLength, fh);
	    return -1;
	  }

	cur = file_position_to_abs (&fh->fh_file_len);
	cur += offset;
      }
    break;
    default:
      abort ();
      return -1;
    }

  if (file_position_to_abs (&fh->fh_cur_pos) != cur)
    {
      FilePosition new_cur;

      if (fh->fh_open_flags & (HV_NoSeek | HV_Replace) || fh->table->fh_read)
	{
	  edsio_generate_handle_event (EC_EdsioIllegalSeek, fh);
	  return -1;
	}

      if (file_position_to_abs (&fh->fh_cur_pos) > file_position_to_abs (& fh->fh_file_len))
	{
	  edsio_generate_handle_event (EC_EdsioIllegalSeek, fh);
	  return -1;
	}

      file_position_from_abs (fh->fh_cur_pos.page_size, cur, &new_cur);

      if (fh->fh_cur_pos.page != new_cur.page && ! handle_unmap_current_page (fh))
	return -1;

      file_position_from_abs (fh->fh_cur_pos.page_size, cur, &fh->fh_cur_pos);
    }

  return cur;
}

gssize
handle_pages (FileHandle *fh)
{
  if (! fh->fh_has_len)
    {
      edsio_generate_handle_event (EC_EdsioHandleHasNoLength, fh);
      return -1;
    }

  return fh->fh_file_len.page + (fh->fh_file_len.offset > 0);
}

gboolean
handle_eof (FileHandle* fh)
{
  if (fh->fh_open_flags & HV_Read)
    {
      if (fh->fh_has_len)
	return file_position_to_abs (& fh->fh_cur_pos) == file_position_to_abs (& fh->fh_file_len);

      g_assert (fh->table->fh_eof);

      return fh->table->fh_eof (fh);
    }
  else
    {
      return TRUE;
    }
}

gssize
handle_length (FileHandle* fh)
{
  if (! fh->fh_has_len)
    {
      edsio_generate_handle_event (EC_EdsioHandleHasNoLength, fh);
      return -1;
    }

  return file_position_to_abs (& fh->fh_file_len);
}

gsize
handle_position (FileHandle* fh)
{
  return file_position_to_abs (& fh->fh_cur_pos);
}

/* Type independent functions
 */

gssize
handle_pagesize (FileHandle *fh)
{
  return fh->fh_cur_pos.page_size;
}

gboolean
handle_putc (FileHandle *fh, gchar c)
{
  return handle_write (fh, (guint8*) &c, 1);
}

gint
handle_getc (FileHandle *fh)
{
  guint8 x;

  if (handle_read (fh, &x, 1) != 1)
    return -1;

  return x;
}

gboolean
handle_getui (FileHandle *fh, guint32* i)
{
  if (handle_read (fh, (guint8*)i, 4) != 4)
    return FALSE;

  *i = g_ntohl (*i);

  return TRUE;
}

gboolean
handle_putui (FileHandle *fh, guint32 i)
{
  guint32 hi = g_htonl (i);

  return handle_write (fh, (guint8*)&hi, 4);
}

gboolean
handle_printf (FileHandle* handle,
	       const char* fmt,
	       ...
	       )
{
  va_list args;
  gboolean result;
  guint8* buf;

  va_start (args, fmt);

  buf = g_strdup_vprintf (fmt, args);

  result = handle_write (handle, buf, strlen (buf));

  g_free (buf);

  va_end (args);

  return result;
}

gpointer
handle_unique (FileHandle* fh)
{
  /* @@@ This can be improved to share buffers between multiple file
   * handles of the same open file, LATER. */
  return fh;
}

gboolean
handle_puts (FileHandle *fh, const char* linebuf)
{
  for (; *linebuf; linebuf += 1)
    {
      if (! handle_putc (fh, *linebuf))
	return FALSE;
    }

  return TRUE;
}

gssize
handle_gets (FileHandle *fh, char* buf, gsize nbyte)
{
  int i = 0;

  for (;;)
    {
      int c;

      if ((c = handle_getc (fh)) < 0)
	break;

      buf[i++] = c;

      if (c == '\n')
	break;

      if (i == nbyte)
	break;
    }

  if (i < nbyte)
    buf[i] = 0;

  return i;
}

gboolean
handle_copy (FileHandle *h1,
	     FileHandle *h2,
	     guint       off,
	     guint       len)
{
  gint pgno;
  guint psize;
  guint pgoff;
  gint onpage, rem;
  const guint8* page;

  if (len == 0)
    return TRUE;

  psize = handle_pagesize (h1);

  pgno = off / psize;
  pgoff = off % psize;

  do
    {
      onpage = handle_map_page (h1, pgno, &page);

      if (onpage <= 0)
	return FALSE;

      rem = MIN (onpage - pgoff, len);

      if (! handle_write (h2, page + pgoff, rem))
	return FALSE;

      len -= rem;

      pgoff = 0;

      if (! handle_unmap_page (h1, pgno++, &page))
	return FALSE;
    }
  while (len > 0);

  return TRUE;
}

/* File position
 */

gssize
file_position_rem_on_page_no (const FilePosition *len, guint page)
{
  if (page > len->page)
    {
      abort ();
      return -1;
    }

  if (page == len->page)
    return len->offset;

  return len->page_size;
}

gssize
file_position_to_abs (const FilePosition *pos)
{
  return pos->page_size * pos->page + pos->offset;
}

void
file_position_from_abs (guint page_size, guint abs, FilePosition *pos)
{
  pos->page_size = page_size;
  pos->page = abs / page_size;
  pos->offset = abs % page_size;
}

void
file_position_incr (FilePosition *pos, gint incr_by)
{
  gint nabs = file_position_to_abs (pos) + incr_by;

  g_assert (nabs >= 0);

  file_position_from_abs (pos->page_size, nabs, pos);
}

/* Stdio
 */

#include "edsiostdio.h"
#include <errno.h>

FileHandle* _stdout_handle;
FileHandle* _stdin_handle;
FileHandle* _stderr_handle;

typedef struct _StdioHandle StdioHandle;

struct _StdioHandle {
  FileHandle handle;

  FILE* file;
  const char* name;
};

static gboolean     stdio_handle_close (FileHandle* fh);
static void         stdio_handle_abort (FileHandle* fh);
static const gchar* stdio_handle_name  (FileHandle *fh);
static void         stdio_handle_free  (FileHandle* fh);
static gssize       stdio_handle_read  (FileHandle *fh, guint8* buf, gsize nbyet);
static gboolean     stdio_handle_write (FileHandle *fh, const guint8 *buf, gsize nbyte);
static gboolean     stdio_handle_eof   (FileHandle *fh);
static gssize       stdio_handle_map   (FileHandle *fh, guint pgno, const guint8** mem);
static gboolean     stdio_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem);

static HandleFuncTable stdio_noseek_table = {
  stdio_handle_close,
  stdio_handle_abort,
  stdio_handle_name,
  stdio_handle_free,
  NULL,
  NULL,
  stdio_handle_read,
  stdio_handle_write,
  stdio_handle_eof
};

static HandleFuncTable stdio_seek_table = {
  stdio_handle_close,
  stdio_handle_abort,
  stdio_handle_name,
  stdio_handle_free,
  stdio_handle_map,
  stdio_handle_unmap,
  NULL,
  NULL,
  NULL
};

#define STDIO_PAGE_SIZE (1<<13)

FileHandle*
handle_read_stdio (FILE* f, const char* name)
{
  StdioHandle *h = g_new0 (StdioHandle, 1);

  struct stat buf;

  h->file = f;
  h->name = name;

  h->handle.fh_open_flags = HV_Read;
  h->handle.fh_open_mode = HV_Read;

  if (fstat (fileno (f), & buf) < 0 || ! S_ISREG (buf.st_mode))
    {
      h->handle.table = & stdio_noseek_table;
      h->handle.fh_open_flags |= HV_NoSeek;
      h->handle.fh_has_len = FALSE;
    }
  else
    {
      h->handle.table = & stdio_seek_table;
      h->handle.fh_open_flags |= HV_NoSeek;
      h->handle.fh_has_len = TRUE;

      file_position_from_abs (STDIO_PAGE_SIZE, buf.st_size, &h->handle.fh_file_len);
    }

  file_position_from_abs (STDIO_PAGE_SIZE, 0, &h->handle.fh_cur_pos);

  return & h->handle;
}

FileHandle*
handle_write_stdio (FILE* f, const char* name)
{
  StdioHandle *h = g_new0 (StdioHandle, 1);

  h->file = f;
  h->name = name;

  h->handle.table = & stdio_noseek_table;
  h->handle.fh_open_flags = HV_Replace;
  h->handle.fh_open_mode = HV_Replace;

  h->handle.fh_has_len = TRUE;

  file_position_from_abs (STDIO_PAGE_SIZE, 0, &h->handle.fh_file_len);
  file_position_from_abs (STDIO_PAGE_SIZE, 0, &h->handle.fh_cur_pos);

  return & h->handle;
}

gboolean
stdio_handle_close (FileHandle* fh)
{
  StdioHandle *s = (StdioHandle*) fh;

  return fclose (s->file) == 0;
}

void
stdio_handle_abort (FileHandle* fh)
{
  g_warning ("cannot abort stdio handles\n");
  abort ();
}

const gchar*
stdio_handle_name  (FileHandle *fh)
{
  StdioHandle *s = (StdioHandle*) fh;

  return g_strdup (s->name);
}

void
stdio_handle_free  (FileHandle* fh)
{
  g_free (fh);
}

gssize
stdio_handle_read  (FileHandle *fh, guint8* buf, gsize nbyte)
{
  StdioHandle *s = (StdioHandle*) fh;
  gssize r;

  if (fh->fh_at_eof)
    return 0;

  clearerr (s->file);

  r = fread (buf, 1, nbyte, s->file);

  if (ferror (s->file))
    {
      edsio_generate_stringerrno_event (EC_EdsioFreadFailed, s->name);
      return -1;
    }

  if (r == nbyte)
    return r;

  if (feof (s->file))
    {
      fh->fh_at_eof = TRUE;
      return r;
    }

  edsio_generate_stringerrno_event (EC_EdsioFreadFailed, s->name);
  return -1;
}

gboolean
stdio_handle_write (FileHandle *fh, const guint8 *buf, gsize nbyte)
{
  StdioHandle *s = (StdioHandle*) fh;
  gssize r;

  clearerr (s->file);

  r = fwrite (buf, 1, nbyte, s->file);

  if (ferror (s->file))
    {
      edsio_generate_stringerrno_event (EC_EdsioFwriteFailed, s->name);
      return FALSE;
    }

  if (r == nbyte)
    return TRUE;

  edsio_generate_stringerrno_event (EC_EdsioFwriteFailed, s->name);
  return FALSE;
}

gboolean
stdio_handle_eof   (FileHandle *fh)
{
  StdioHandle *s = (StdioHandle*) fh;

  if (fh->fh_at_eof)
    return TRUE;

  if (feof (s->file))
    {
      fh->fh_at_eof = TRUE;
      return TRUE;
    }

  return FALSE;
}

gssize
stdio_handle_map (FileHandle *fh, guint pgno, const guint8** mem)
{
  StdioHandle *s = (StdioHandle*) fh;
  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  if (res == 0)
    return res;

  if (! ((*mem) = mmap (NULL, res, PROT_READ, MAP_PRIVATE, fileno (s->file), pgno * STDIO_PAGE_SIZE)))
    {
      edsio_generate_handleint_event (EC_EdsioMmapFailed, fh, errno);
      return -1;
    }

  return res;
}

gboolean
stdio_handle_unmap (FileHandle *fh, guint pgno, const guint8** mem)
{
  gssize res = file_position_rem_on_page_no (& fh->fh_file_len, pgno);

  if (res > 0 && munmap ((void*) *mem, res) < 0)
    {
      edsio_generate_handleint_event (EC_EdsioMunmapFailed, fh, errno);
      return FALSE;
    }

  (*mem) = NULL;

  return TRUE;
}

gboolean
edsio_stdio_init (void)
{
  if (! (_stdout_handle = handle_write_stdio (stdout, "<standard output>")))
    return FALSE;

  if (! (_stderr_handle = handle_write_stdio (stderr, "<standard error>")))
    return FALSE;

  if (! (_stdin_handle  = handle_read_stdio (stdin, "<standard input>")))
    return FALSE;

  return TRUE;
}

gboolean
handle_drain (FileHandle *from,
	      FileHandle *to)
{
  gint pgno = from->fh_cur_pos.page;
  guint psize;
  gint onpage;
  const guint8* page;

  psize = handle_pagesize (from);

  while (pgno < handle_pages (from))
    {
      onpage = handle_map_page (from, pgno, &page);

      if (onpage < 0)
	return FALSE;

      if (onpage > 0 && ! handle_write (to, page, onpage))
	return FALSE;

      if (! handle_unmap_page (from, pgno++, &page))
	return FALSE;
    }

  return TRUE;
}

FileHandle*
handle_read_file (const char* name)
{
  FILE* f;

  if (! (f = fopen (name, "r")))
    return NULL;

  return handle_read_stdio (f, name);
}

FileHandle*
handle_write_file (const char* name)
{
  FILE* f;

  if (! (f = fopen (name, "w")))
    return NULL;

  return handle_write_stdio (f, name);
}
